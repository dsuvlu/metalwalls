! ================================================================================
! Update block boundaries for block loops in self-other interaction
subroutine update_other_block_boundaries(iblock, &
      offset_n, count_n, offset_m, count_m, &
      istart, iend, jstart, jend)
   use MW_constants, only: pair_block_size
   implicit none
   ! Parameters
   ! ----------
   integer, intent(in) :: iblock      ! block index
   integer, intent(in) :: offset_n    ! offset for groups of particules in rows
   integer, intent(in) :: count_n     ! count of particles in rows
   integer, intent(in) :: offset_m    ! offset for groups of particules in columns
   integer, intent(in) :: count_m     ! count of particles in columns

   integer, intent(inout) :: istart   ! current start index of block
   integer, intent(inout) :: iend     ! current end index of block
   integer, intent(inout) :: jstart   ! current start index of block
   integer, intent(inout) :: jend     ! current end index of block

   ! Local
   ! -----
   integer :: block_col_num
   integer :: block_col, block_row

   block_col_num = (count_m-1) / pair_block_size + 1

   block_row = ((iblock-1) / block_col_num) + 1
   block_col = mod(iblock-1, block_col_num) + 1

   istart = (block_row-1)*pair_block_size + offset_n + 1
   iend = min(count_n+offset_n, istart + pair_block_size - 1)
   jstart = (block_col-1)*pair_block_size + offset_m + 1
   jend = min(count_m+offset_m, jstart + pair_block_size - 1)
end subroutine update_other_block_boundaries
