module MW_dipoles_and_electrodes
  use MW_stdio
  use MW_kinds, only: wp
  use MW_algorithms, only: MW_algorithms_t
  use MW_cg, only: MW_cg_t
  use MW_system, only: MW_system_t
  use MW_ion, only: MW_ion_t
  use MW_electrode, only: MW_electrode_t
  use MW_external_field, only: MW_external_field_t, &
       FIELD_TYPE_ELECTRIC, FIELD_TYPE_DISPLACEMENT

  implicit none
  private

  ! Public subroutines
  ! ------------------
  public :: compute

contains

  !================================================================================
  ! Evaluate b, the potential on electrode atoms due to melt ions
  subroutine setup_b(system, algorithms)
    use MW_coulomb, only: &
         MW_coulomb_qmelt2Qelec_potential => qmelt2Qelec_potential ,&
         MW_coulomb_qmelt2mumelt_electricfield => qmelt2mumelt_electricfield ,&
         MW_coulomb_fix_molecule_qmelt2mumelt_electricfield => fix_molecule_qmelt2mumelt_electricfield
    use MW_external_field, only: &
         MW_external_field_field2mumelt_electricfieldb => field2mumelt_electricfieldb, &
         MW_external_field_Efield_elec_potential => Efield_elec_potential, &
         MW_external_field_Dfield_elec_potentialb => Dfield_elec_potentialb
    implicit none
    ! Parameters
    ! ----------
    type(MW_system_t), intent(inout) :: system
    type(MW_algorithms_t), intent(inout) :: algorithms

    ! Locals
    ! ------
    integer :: num_ions, num_atoms
    integer :: xyz_now, dipoles_now, qatoms_now
    integer :: i, itype, ix, iy, iz, ei
    real(wp) :: alpha
    real(wp), dimension(system%num_ions,3) :: efield, efield2, efield3
    real(wp) :: Vi
    real(wp), dimension(system%num_atoms,4) :: pot_atoms
    real(wp), dimension(system%num_atoms) :: pot_field


    num_ions = system%num_ions
    num_atoms = system%num_atoms
    xyz_now = system%xyz_ions_step(1)
    dipoles_now = system%dipoles_ions_step(1)
    qatoms_now = system%q_atoms_step(1)

    efield=0.0_wp
    call MW_coulomb_qmelt2mumelt_electricfield(system%num_PBC, system%localwork, system%ewald, system%box, &
    system%ions, system%xyz_ions(:,:,xyz_now), efield, system%tt)

    efield2=0.0_wp
    if(system%num_molecule_types > 0)then
      call MW_coulomb_fix_molecule_qmelt2mumelt_electricfield(system%num_PBC, system%localwork, &
      system%molecules, system%box, system%ions, system%xyz_ions(:,:,xyz_now), efield2, system%tt)
    endif

    efield3=0.0_wp
    if (system%field%field_type > 0) then
       call MW_external_field_field2mumelt_electricfieldb(system%field, system%ions,&
       system%polarization_field, efield3)
    endif

    efield=efield+efield2+efield3
    do itype = 1,size(system%ions,1)
      alpha = system%ions(itype)%polarizability
      do i=system%ions(itype)%offset+1,system%ions(itype)%offset+system%ions(itype)%count
          ix = i
          iy = i + num_ions
          iz = i + num_ions*2
          if(algorithms%maze%calc_dip) then
             algorithms%maze%const_constr(ix) = -efield(i,1)
             algorithms%maze%const_constr(iy) = -efield(i,2)
             algorithms%maze%const_constr(iz) = -efield(i,3)
          else
             algorithms%cg%b(ix) = alpha*efield(i,1)
             algorithms%cg%b(iy) = alpha*efield(i,2)
             algorithms%cg%b(iz) = alpha*efield(i,3)
          end if
      end do
    end do

    pot_atoms=0.0_wp
    call MW_coulomb_qmelt2Qelec_potential(system%num_PBC, system%localwork, &
         system%ewald, system%box, system%ions, system%xyz_ions(:,:,xyz_now), &
         system%electrodes, system%xyz_atoms, pot_atoms)

    pot_field=0.0_wp
    if (system%field%field_type == FIELD_TYPE_ELECTRIC) then
       call MW_external_field_Efield_elec_potential(system%field, &
            system%electrodes, system%xyz_atoms, pot_field)
    else if (system%field%field_type == FIELD_TYPE_DISPLACEMENT) then
       call MW_external_field_Dfield_elec_potentialb(system%field, &
            system%electrodes, system%xyz_atoms, system%polarization_field, pot_field)
    endif

    do ei = 1, system%num_elec                ! loop over each electrode wall
       Vi = system%electrodes(ei)%V           ! potential applied on the electrode
       do i = system%electrodes(ei)%offset+1, system%electrodes(ei)%offset + system%electrodes(ei)%count
          if (algorithms%maze%calc_elec) then
             algorithms%maze%const_constr(3*num_ions + i) = (-Vi + &
                  (pot_atoms(i,1) + pot_atoms(i,2) + pot_atoms(i,3) + pot_field(i)))
          else
             algorithms%cg%b(3*num_ions + i) = Vi - &
                  (pot_atoms(i,1) + pot_atoms(i,2) + pot_atoms(i,3) + pot_field(i))
          end if
       end do
    end do
  end subroutine setup_b

  !================================================================================
  ! Compute A*x, the potential on electrode atoms due to electrode atoms
  !
  subroutine apply_A(system, x, y)
    use MW_coulomb, only: &
         MW_coulomb_mumelt2Qelec_potential => mumelt2Qelec_potential, &
         MW_coulomb_Qelec2Qelec_potential => Qelec2Qelec_potential, &
         MW_coulomb_mumelt2mumelt_electricfield => mumelt2mumelt_electricfield, &
         MW_coulomb_Qelec2mumelt_electricfield => Qelec2mumelt_electricfield, &
         MW_coulomb_fix_molecule_mumelt2mumelt_electricfield => fix_molecule_mumelt2mumelt_electricfield
    use MW_external_field, only: &
         MW_external_field_field2mumelt_electricfieldA => field2mumelt_electricfieldA, &
         MW_external_field_Dfield_elec_potentialA => Dfield_elec_potentialA
    implicit none
    ! Parameters
    ! ----------
    type(MW_system_t), intent(inout) :: system
    real(wp), intent(in) :: x(:)
    real(wp), intent(out) :: y(:)

    ! Locals
    ! ------
    integer :: num_ions, num_atoms
    integer :: i, itype, ix, iy, iz
    integer :: xyz_now
    real(wp) :: alpha
    real(wp), dimension(system%num_ions,3) :: efield, efield2, efield3, efield4
    real(wp), dimension(system%num_ions,3) :: dipoles
    real(wp), dimension(system%num_atoms,4) :: pot_atoms, pot_atoms2
    real(wp), dimension(system%num_atoms) :: q_atoms, pot_field

    num_ions = system%num_ions
    num_atoms = system%num_atoms

    do i=1,num_ions
      ix = i
      iy = i + num_ions
      iz = i + num_ions*2
      dipoles(i,1) = x(ix)
      dipoles(i,2) = x(iy)
      dipoles(i,3) = x(iz)
    end do
    do i=1,num_atoms
      q_atoms(i) = x(3*num_ions+i)
    end do

    xyz_now=system%xyz_ions_step(1)


    efield=0.0_wp
    call MW_coulomb_mumelt2mumelt_electricfield(system%num_PBC, system%localwork, system%ewald, system%box, &
         system%ions, system%xyz_ions(:,:,xyz_now), efield, dipoles)

    efield2=0.0_wp
    if(system%num_molecule_types > 0) then
      call MW_coulomb_fix_molecule_mumelt2mumelt_electricfield(system%num_PBC, system%localwork, &
            system%molecules, system%box, system%ions, system%xyz_ions(:,:,xyz_now), efield2, dipoles)
    endif

    ! UNCOMMENT AND PUT RIGHT INPUT/OUTPUT VARIABLES
    efield3=0.0_wp
    call MW_coulomb_Qelec2mumelt_electricfield(system%num_PBC, system%localwork, system%ewald, system%box, &
         system%ions, system%xyz_ions(:,:,xyz_now), system%electrodes, system%xyz_atoms, &
         q_atoms, efield3)

    efield4=0.0_wp
    if (system%field%field_type == FIELD_TYPE_DISPLACEMENT) then
       call MW_external_field_field2mumelt_electricfieldA(system%box, system%field, system%electrodes, &
       system%xyz_atoms, q_atoms, system%ions, dipoles, system%polarization_field, efield4)
    endif

    efield=efield+efield2+efield3+efield4
    do itype = 1,size(system%ions,1)
      alpha = system%ions(itype)%polarizability
      do i=system%ions(itype)%offset+1,system%ions(itype)%offset+system%ions(itype)%count
          ix = i
          iy = i + num_ions
          iz = i + num_ions*2
          y(ix) = dipoles(i,1)-alpha*efield(i,1)
          y(iy) = dipoles(i,2)-alpha*efield(i,2)
          y(iz) = dipoles(i,3)-alpha*efield(i,3)
      end do
    end do

    ! UNCOMMENT AND PUT RIGHT INPUT/OUTPUT VARIABLES
    pot_atoms=0.0_wp
    call MW_coulomb_mumelt2Qelec_potential(system%num_PBC, system%localwork, system%ewald, system%box, &
         system%ions, system%xyz_ions(:,:,xyz_now), system%electrodes, system%xyz_atoms, dipoles, pot_atoms)

    pot_atoms2=0.0_wp
    call MW_coulomb_Qelec2Qelec_potential(system%num_PBC, system%localwork, system%ewald, system%box, &
         system%electrodes, system%xyz_atoms, q_atoms, pot_atoms2)

    pot_field=0.0_wp
    ! Compute the contribution due to the external electric displacement
    if (system%field%field_type == FIELD_TYPE_DISPLACEMENT) then
       call MW_external_field_Dfield_elec_potentialA(system%box, system%field, &
            system%electrodes, system%xyz_atoms, q_atoms, system%polarization_field, pot_field)
    endif

    pot_atoms=pot_atoms+pot_atoms2
    do i = 1, num_atoms
       y(3*num_ions+i) = (pot_atoms(i,1) + pot_atoms(i,2) &
            +pot_atoms(i,3) + pot_atoms(i,4) + pot_field(i))
    end do
  end subroutine apply_A

  !================================================================================
  !> Compute charges on electrode
  !!
  !! The charge is evaluated by minimizing the Coulomb energy of the system
  !!
  !! The energy can be cast into E = Q*AQ - Q*b + c
  !!
  !! where Q  is the vector of charges on the electrodes
  !!       b  is the potential felt by the electrode due to the melt ions
  !!       AQ is the potential felt by the electrode du to other electrode atoms
  !!       c  is the energy due to melt ions interactions
  !!       *  denotes the transpose of a vector/matrix
  !!
  !! By construction A is a matrix symmetric positive definite
  !! The preconditioned conjugate gradient algorithm is used to find a solution to
  !! the system
  !!
  subroutine compute(system, algorithms)
    use MW_cg, only: MW_cg_solve => solve
    ! use MW_maze, only: &
         ! MW_maze_update_history => update_history, &
         ! MW_maze_compute_x0 => compute_x0, &
         ! MW_maze_compute_gradient => compute_gradient, &
         ! MW_maze_solve => solve
    use MW_external_field, only: &
         MW_external_field_elec_polarization => elec_polarization, &
         MW_external_field_dipole_polarization => dipole_polarization
    implicit none

    ! Parameters in
    ! -------------
    type(MW_system_t), intent(inout) :: system !< system parameters
    type(MW_algorithms_t), intent(inout) :: algorithms !< cg algorithm parameters

    ! Local
    ! -----
    integer :: i, itype, ix, iy, iz
    integer :: num_ions, num_atoms
    integer :: q_atoms_tmdt, q_atoms_t, q_atoms_tpdt
    integer :: dipoles_ions_tpdt, dipoles_ions_t, dipoles_ions_tmdt
    real(wp), dimension(3*system%num_ions+system%num_atoms) :: x
    real(wp), dimension(3*system%num_ions+system%num_atoms) :: xt, xtmdt


    num_ions = system%num_ions
    num_atoms = system%num_atoms
    dipoles_ions_tpdt = system%dipoles_ions_step(1)
    q_atoms_tpdt = system%q_atoms_step(1)
    dipoles_ions_t = system%dipoles_ions_step(2)
    q_atoms_t = system%q_atoms_step(2)
    if (algorithms%maze%calc_elec.and.algorithms%maze%calc_dip) then
      dipoles_ions_tmdt = system%dipoles_ions_step(3)
      q_atoms_tmdt = system%q_atoms_step(3)
    end if

    do i=1,num_ions
      ix = i
      iy = i + num_ions
      iz = i + num_ions*2
      x(ix)=system%dipoles_ions(i,1,dipoles_ions_tpdt)
      x(iy)=system%dipoles_ions(i,2,dipoles_ions_tpdt)
      x(iz)=system%dipoles_ions(i,3,dipoles_ions_tpdt)
      xt(ix)=system%dipoles_ions(i,1,dipoles_ions_t)
      xt(iy)=system%dipoles_ions(i,2,dipoles_ions_t)
      xt(iz)=system%dipoles_ions(i,3,dipoles_ions_t)
      if (algorithms%maze%calc_dip.and.algorithms%maze%calc_elec) then
          xtmdt(ix)=system%dipoles_ions(i,1,dipoles_ions_tmdt)
          xtmdt(iy)=system%dipoles_ions(i,2,dipoles_ions_tmdt)
          xtmdt(iz)=system%dipoles_ions(i,3,dipoles_ions_tmdt)
      end if
    end do

    do i=1,num_atoms
      x (3*num_ions+i)=system%q_atoms(i, q_atoms_tpdt)
      xt(3*num_ions+i)=system%q_atoms(i, q_atoms_t)
      if (algorithms%maze%calc_dip.and.algorithms%maze%calc_elec) then
         xtmdt(3*num_ions+i)=system%q_atoms(i, q_atoms_tmdt)
      end if
    end do

    if (algorithms%constant_charge) then
       do itype = 1, system%num_elec
          do i = system%electrodes(itype)%offset+1, system%electrodes(itype)%offset + system%electrodes(itype)%count
             system%q_atoms(i, q_atoms_tpdt) = system%q_atoms(i, q_atoms_t)
          end do
       end do
    else

       if (algorithms%maze%calc_elec.and.algorithms%maze%calc_dip) then

          ! call setup_b(system, algorithms)
          ! call MW_maze_update_history(algorithms%maze) !Only updates gradient of constraints
          ! call MW_maze_compute_x0(algorithms%maze, 1.0_wp, xtmdt, xt, x)
          ! call MW_maze_compute_gradient(algorithms%maze, system)
          ! call MW_maze_solve(algorithms%maze, system, x)

       else

          ! if (system%electrode_use_predictor) then
          ! predictor not yet implemented for coupled system
          ! if (.false.) then
          !    call predictor(system%q_atoms_step, system%electrodes, system%q_atoms)
          ! else
             ! Initialize with previous step value
             ! x = xt
          ! end if

          ! Enforce charge neutrality constraint on initial vector
          ! if (system%electrode_charge_neutrality) then
          !    total_charges = sum(system%q_atoms(:, q_atoms_tpdt))
          !    average_charges = total_charges / real(system%num_atoms, wp)
          !    do itype = 1, system%num_elec
          !       do i = system%electrodes(itype)%offset+1, system%electrodes(itype)%offset + system%electrodes(itype)%count
          !          system%q_atoms(i, q_atoms_tpdt) = system%q_atoms(i, q_atoms_tpdt) - average_charges
          !       end do
          !    end do
          ! end if

          x = xt

          call setup_b(system, algorithms)

          call MW_cg_solve(algorithms%cg, system, apply_A, x)

          ! compute \ksi = V_i - \frac{\partial U_{es}}{\partial q_i}
          ! cg%b stores V_i - \frac{\partial U_{es}}{\partial q_i} for melt->elec interactions
          ! apply_A(system, system%q_atoms(:, q_atoms_now), cg%Ap) computes \frac{\partial U_{es}}{\partial q_i} for elec<->elec interactions
          ! ksi(:) is stored in cg%Ap(:)
          ! if (system%electrode_charge_neutrality) then
          !    call apply_A(system, system%q_atoms(:, q_atoms_tpdt), algorithms%cg%Ap)
          !    do itype = 1, system%num_elec
          !       do i = system%electrodes(itype)%offset+1, system%electrodes(itype)%offset + system%electrodes(itype)%count
          !          algorithms%cg%Ap(i) = algorithms%cg%b(i) - algorithms%cg%Ap(i)
          !       end do
          !    end do
          ! end if
       end if

       do i=1,num_ions
          ix = i
          iy = i + num_ions
          iz = i + num_ions*2
          system%dipoles_ions(i,1,dipoles_ions_tpdt) = x(ix)
          system%dipoles_ions(i,2,dipoles_ions_tpdt) = x(iy)
          system%dipoles_ions(i,3,dipoles_ions_tpdt) = x(iz)
       end do
       do i=1,num_atoms
          system%q_atoms(i, q_atoms_tpdt) = x(3*num_ions+i)
       end do

    end if

    ! update the contribution of the electrode and the dipoles to the polarization
    call MW_external_field_elec_polarization(system%box, system%electrodes(:), system%xyz_atoms(:,:), &
          system%q_atoms(:, q_atoms_tpdt), system%polarization_field(:,:))
    call MW_external_field_dipole_polarization(system%box, system%ions, &
          system%dipoles_ions(:,:,dipoles_ions_tpdt), system%polarization_field(:,:))

  end subroutine compute

  ! ================================================================================
  !> Predictor for wall atoms charges
  subroutine predictor(q_atoms_step, electrodes, q_atoms)
    implicit none
    ! Parameters
    ! ----------
    integer, intent(in) :: q_atoms_step(:)  !< time step index
    type(MW_electrode_t), intent(in) :: electrodes(:) !< electrode parameters
    real(wp), intent(inout) :: q_atoms(:,:) !< atom charges

    ! Local
    ! -----
    integer :: q_now, q_m1, q_m2, q_m3, q_m4, q_m5, q_m6
    integer :: num_atom_types
    integer :: itype, i

    real(wp), parameter :: B1 =  22.0_wp /  7.0_wp
    real(wp), parameter :: B2 = -55.0_wp / 14.0_wp
    real(wp), parameter :: B3 =  55.0_wp / 21.0_wp
    real(wp), parameter :: B4 = -22.0_wp / 21.0_wp
    real(wp), parameter :: B5 =   5.0_wp / 21.0_wp
    real(wp), parameter :: B6 =  -1.0_wp / 42.0_wp

    num_atom_types = size(electrodes,1)
    q_now = q_atoms_step(1)
    q_m1 = q_atoms_step(2)
    q_m2 = q_atoms_step(3)
    q_m3 = q_atoms_step(4)
    q_m4 = q_atoms_step(5)
    q_m5 = q_atoms_step(6)
    q_m6 = q_atoms_step(7)

    do itype = 1, num_atom_types
       do i = electrodes(itype)%offset+1, electrodes(itype)%offset + electrodes(itype)%count
          q_atoms(i,q_now) = B1 * q_atoms(i,q_m1) + B2 * q_atoms(i,q_m2) + B3 * q_atoms(i,q_m3) &
               + B4 * q_atoms(i, q_m4) + B5 * q_atoms(i,q_m5) + B6 * q_atoms(i, q_m6)
       end do
    end do
  end subroutine predictor

end module MW_dipoles_and_electrodes
