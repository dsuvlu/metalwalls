# Compilation options
F90_PREFIX := 
F90 := $(F90_PREFIX)mpif90
F90STDFLAGS := -g
F90OPTFLAGS := -O2 -xCORE-AVX512 -align array64byte -fPIC -DMW_USE_PLUMED
F90REPORTFLAGS := 
F90FLAGS := $(F90STDFLAGS) $(F90OPTFLAGS) $(F90REPORTFLAGS)
FPPFLAGS := -fpp
LDFLAGS := $(LAPACK_LDFLAGS)
J := -module 
# Path to pFUnit (Unit testing Framework)
PFUNIT := $(ALL_CCCHOME)/opt/pfunit/pfunit-parallel
F90WRAP := f90wrap
F2PY := f2py3.7
FCOMPILER := intelem
#CCOMPILER := intelem
PLUMED_LOAD := $(LIB_PLUMED_LDFLAGS)
