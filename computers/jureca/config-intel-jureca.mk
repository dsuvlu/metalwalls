# Compilation options
F90_PREFIX := 
F90 := $(F90_PREFIX) mpif90
F90STDFLAGS := -g
F90OPTFLAGS := -O2 -xHost -align array64byte
F90REPORTFLAGS := -qopt-report-phase=vec -qopt-report=2
F90FLAGS := $(F90STDFLAGS) $(F90OPTFLAGS) $(F90REPORTFLAGS)
FPPFLAGS := -fpp
LDFLAGS := -llapack
J := -module 
# Path to pFUnit (Unit testing Framework)
PFUNIT := $(HOME)/opt/pfunit/pfunit-parallel
