# Compilation options
F90 := mpiifort
F90STDFLAGS := -g -mkl=cluster -fPIC 
F90OPTFLAGS := -O2 -xCORE-AVX512 -align array64byte 
F90REPORTFLAGS := 
F90FLAGS := $(F90STDFLAGS) $(F90OPTFLAGS) $(F90REPORTFLAGS)
FPPFLAGS := -DMW_CI -DMW_USE_PLUMED
LDFLAGS := 
F2PY := f2py
F90WRAP := f90wrap
FCOMPILER := intelem
J := -module 
# Path to pFUnit (Unit testing Framework)
PFUNIT := $(PFUNIT)
