# Compilation options
F90 := mpiifort
F90FLAGS := -g -O2 -xCORE-AVX512 -mkl=cluster -align array64byte
FPPFLAGS := -fpp
LDFLAGS := 
F2PY := f2py-f90wrap
F90WRAP := f90wrap
FCOMPILER := intelem
CCOMPILER := intelem
J := -module 
