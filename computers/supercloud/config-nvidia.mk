F90 := mpif90
F90STDFLAGS := -g -Minform=info -Mallocatable=95
F90OPTFLAGS := -O2 -fastsse -Mvect=simd:512 -Mscalapack -llapack -fortranlibs -traceback
F90REPORTFLAGS := 
F90FLAGS := $(F90STDFLAGS) $(F90OPTFLAGS) $(F90REPORTFLAGS)
FPPFLAGS := -Mpreprocess
LDFLAGS := 
#F2PY := f2py-f90wrap
#F90WRAP := f90wrap
#FCOMPILER := nv
#CCOMPILER := intelem
J := -module

