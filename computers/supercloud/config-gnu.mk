# Compilation options
F90 := mpif90
F90FLAGS := -O3 -g -march=skylake-avx512 -mtune=skylake-avx512 -fopenmp -fPIC
FPPFLAGS := -cpp -DMW_USE_PLUMED
LDFLAGS := -llapack
F2PY := f2py
F90WRAP := f90wrap
FCOMPILER := gnu95
J := -J

