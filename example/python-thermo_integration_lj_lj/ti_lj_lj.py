import numpy as np
import mw

# Thermodynamic integration between two sets of LJ parameters

# Setup LJ parameters
# Define parameters matrices for LJ interactions
ntypes = 4
# Parameters of initial state
eps1 = np.zeros((ntypes, ntypes))
sigma1 = np.zeros((ntypes, ntypes))
# O, O
eps1[0,0] = 0.6501655
sigma1[0,0] = 3.16555789
# O, ION
eps1[0,3] = 0.521578
sigma1[0,3] = 2.876500
# ION, ION not computed (only 1 ion)

# Parameters of final state
eps2 = np.zeros((ntypes, ntypes))
sigma2 = np.zeros((ntypes, ntypes))
# O, O
eps2[0,0] = 0.6501655
sigma2[0,0] = 3.16555789
# O, ION
eps2[0,3] = 0.5216
sigma2[0,3] = 3.25
# ION, ION not computed (only 1 ion)

# List of lambda (l) values for the thermodynamic integration
# Defined as U(l) = (1-l) U1 + l U2
lambd_list = np.arange(0, 1.1, 0.1)

# Evaluate energy derivative every num_steps_per_cycle, 
# for num_cycles cycles
num_steps_per_cycle = 10
num_cycles = 100

# Run the thermodynamic integration
mw.thermodynamic_integration.ti_lj_lj(eps1, sigma1, eps2, sigma2, lambd_list, num_steps_per_cycle, num_cycles, 'energy_derivative.out')

# Plot the results
mw.thermodynamic_integration.ti_plot('energy_derivative.out', 'ti_lj_lj.png')
