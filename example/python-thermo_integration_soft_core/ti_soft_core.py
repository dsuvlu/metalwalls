import numpy as np
import mw

# Setup LJ parameters !Python indexes
ntypes = 4
eps1 = np.zeros((ntypes, ntypes))
eps2 = np.zeros((ntypes, ntypes))
sigma1 = np.zeros((ntypes, ntypes))
sigma2 = np.zeros((ntypes, ntypes))

# O, O
eps1[0,0] = 0.6501655
sigma1[0,0] = 3.16555789
# O, ION
eps1[0,3] = 0.0
sigma1[0,3] = 2.876500
# ION, ION not computed (only 1 ion)

# O, O
eps2[0,0] = 0.6501655
sigma2[0,0] = 3.16555789
# O, ION
eps2[0,3] = 0.521578
sigma2[0,3] = 2.876500
# ION, ION not computed (only 1 ion)

lambd_list = np.arange(0, 1.1, 0.1)
num_steps_per_cycle = 100
num_cycles = 100
ion_type = 4 # Fortran indexes

# Soft-core parameters
p = 1
alpha = 0.5

mw.thermodynamic_integration.ti_soft_core(eps1, sigma1, eps2, sigma2, ion_type, p, alpha, lambd_list, num_steps_per_cycle, num_cycles, output_file='energy_derivative.out')

mw.thermodynamic_integration.ti_plot('energy_derivative.out', 'ti_soft_core.png')
