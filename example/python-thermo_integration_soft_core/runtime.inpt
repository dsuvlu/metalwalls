# System configuration
# ====================

# Global simulation parameters
# ----------------------------
timestep     41.341 # timestep (a.u)
temperature   298.0 # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  3

# Thermostat:
# -----------
thermostat
   relaxation_time 10000
   tolerance 1e-17

# Species definition
# ---------------
species

  species_type
    name   O               # name of the species
    count  299             # number of species
    mass   15.996          # mass in amu
    mobile True
    charge point -0.8476   # permanent charge on the ions

  species_type
    name   H1              # name of the species
    count  299             # number of species
    mass   1.0008          # mass in amu
    mobile True
    charge point 0.4238    # permanent charge on the ions

  species_type
    name   H2              # name of the species
    count  299             # number of species 
    mass   1.0008          # mass in amu
    mobile True
    charge point 0.4238    # permanent charge on the ions

  species_type
    name   ION             # name of the species
    count  1               # number of species 
    mass   22.990          # mass in amu
    mobile True
    charge point 0.000     # permanent charge on the ions


# Molecule definitions
# --------------------
molecules

  molecule_type
    name SPC         # name of molecule
    count 299        # number of molecules
    sites O H1 H2    # molecule's sites

    # Rigid constraints
    constraint  O H1 1.88973 # constrained radius for a pair of sites (bohr)
    constraint  O H2 1.88973 # constrained radius for a pair of sites (bohr)
    constraint H1 H2 3.08589 # constrained radius for a pair of sites (bohr)

    constraints_algorithm rattle 1.0e-7 100


# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol       2.08321e-05      # coulomb rtol
    coulomb_rcut       18.89726124565   # coulomb cutoff (bohr)
    coulomb_ktol       1.e-7            # coulomb ktol

  lennard-jones
     lj_rcut 18.8972612456506           # lj cutoff (bohr)
     # lj parameters: epsilon in kJ/mol, sigma in angstrom
     lj_pair   O       O    0.6501655     3.16555789  
     lj_pair   O       ION  0.521578      2.876500


# Output section
# --------------
output
  default 0
  xyz 100
  step 10
