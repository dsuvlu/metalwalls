units               real    
dimension           3   
newton              on 
boundary            p p f 
atom_style          full 

#  Atom definition
read_data capacitor.lmp

variable CAB equal 1
variable CAC equal 2
variable CG equal 3
variable NAA equal 4

# Group
group electrode1 molecule 81
group electrode2 molecule 82
group electrode type ${CG}
group mobile type ${CAB} ${CAC} ${NAA}

set type ${CAB} charge 0.129
set type ${CAC} charge 0.269
set type ${NAA} charge -0.398
set group electrode1 charge 0.01
set group electrode2 charge -0.01

# Force field
pair_style lj/cut/coul/long 8.0 
kspace_style ewald 1.0e-8
kspace_modify slab 4.0
pair_modify shift yes

pair_coeff ${CAB}   ${CAB}      0.09935850       3.40000000  # CAB CAB
pair_coeff ${CAB}   ${CAC}      0.19419428       3.50000000  # CAB CAC
pair_coeff ${CAB}   ${CG}       0.07390356       3.38500000  # CAB CG
pair_coeff ${CAB}   ${NAA}      0.09935850       3.35000000  # CAB NAA
pair_coeff ${CAC}   ${CAC}      0.37954900       3.60000000  # CAC CAC
pair_coeff ${CAC}   ${CG}       0.14444310       3.48500000  # CAC CG
pair_coeff ${CAC}   ${NAA}      0.19419428       3.45000000  # CAC NAA
pair_coeff ${CG}    ${CG}       0.0 1.0 #0.05497000       3.37000000  # CG CG
pair_coeff ${CG}    ${NAA}      0.07390356       3.33500000  # CG NAA
pair_coeff ${NAA}   ${NAA}      0.09935850       3.30000000  # NAA NAA
pair_coeff * * 0.0 1.0 

write_data ff.dat pair ij
neigh_modify check yes delay 0 every 1

#  Fixes, thermostats and barostats / Run
timestep 1

dump out1 all custom 1 traj.lammpstrj id element x y z fx fy fz
dump_modify out1 element CAB CAC CG NAA sort id format float %20.15g

dump out mobile custom 1 forces.ref id element x y z fx fy fz
dump_modify out element CAB CAC CG NAA sort id format float %20.15g

dump out2 electrode custom 1 elec_forces.ref id element fx fy fz
dump_modify out2 element CAB CAC CG NAA sort id format float %20.15g

velocity all set 0.0 0.0 0.0

thermo_style custom step temp epair emol ke pe etotal cella cellb cellc
thermo 100

compute fz1 electrode1 reduce sum fz
compute fz2 electrode2 reduce sum fz
compute fy1 electrode1 reduce sum fy
compute fy2 electrode2 reduce sum fy
compute fx1 electrode1 reduce sum fx
compute fx2 electrode2 reduce sum fx

#variable nc equal count(electrode1)
#variable surface equal cella*cellb
#variable pressure equal 0.0
#variable conversion equal 101325*6.02214076*(10^-10)/4.184
#variable fp1 equal ${pressure}*${conversion}*${surface}
#variable fp2 equal -${pressure}*${conversion}*${surface}

#variable f1 equal ${fp1}/${nc}+c_fz1/${nc}
#variable f2 equal ${fp2}/${nc}+c_fz2/${nc}
#variable f1 equal ${fp1}+c_fz1
#variable f2 equal ${fp2}+c_fz2
variable f1z equal c_fz1
variable f2z equal c_fz2
variable f1y equal c_fy1
variable f2y equal c_fy2
variable f1x equal c_fx1
variable f2x equal c_fx2

fix drag1 electrode1 setforce v_f1x v_f1y v_f1z 
fix drag2 electrode2 setforce v_f2x v_f2y v_f2z

fix integrator all nve

run 0
